package com.web.wps.util.upload.minio;

import com.web.wps.propertie.MinioProperties;
import com.web.wps.util.file.FileType;
import com.web.wps.util.file.FileTypeJudge;
import com.web.wps.util.file.FileUtil;
import com.web.wps.util.upload.FileInterface;
import com.web.wps.util.upload.ResFileDTO;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Component("minio")
@SuppressWarnings("ALL")
public class MinioUtil implements FileInterface {
    private final MinioProperties minio;

    private final FileTypeJudge fileTypeJudge;

    private final  MinioClient minioClient;

    @Autowired
    public MinioUtil(MinioProperties minio, FileTypeJudge fileTypeJudge, MinioClient minioClient) {
        this.minio = minio;
        this.fileTypeJudge = fileTypeJudge;
        this.minioClient = minioClient;
    }


    @Override
    public void deleteFile(String key) {
        try {
            // 单个删除
            String removeKey = this.minio.getDiskName() + key;
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(this.minio.getBucketName()).object(removeKey).build());
            // 批量删除
//            List<DeleteObject> dos = Stream.of(removeKey).map(DeleteObject::new).collect(Collectors.toList());
//            minioClient.removeObjects(RemoveObjectsArgs.builder().bucket(this.minio.getBucketName()).objects(dos).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String uploadFile(InputStream inputStream, String fileName, long fileSize, String bucketName, String diskName, String localFileName) {
        String resultStr = null;
        try {
            ObjectWriteResponse objectWriteResponse = minioClient.putObject(PutObjectArgs.builder()
                    .bucket(bucketName)
                    .stream(inputStream, inputStream.available(), -1L)
                    .object(fileName)
                    .bucket(bucketName)
                    .build());
            resultStr = objectWriteResponse.etag();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultStr;
    }

    @Override
    public ResFileDTO uploadMultipartFile(MultipartFile file) {
        String fileType, fileName = file.getOriginalFilename();
        ResFileDTO o = new ResFileDTO();
        long fileSize = file.getSize();
        try {
            InputStream inputStream = file.getInputStream();
            FileType type = this.fileTypeJudge.getType(inputStream);
            if (type == null || "null".equals(type.toString()) ||
                    "XLS_DOC".equals(type.toString()) || "XLSX_DOCX".equals(type.toString()) ||
                    "WPSUSER".equals(type.toString()) || "WPS".equals(type.toString())) {
                fileType = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            } else {
                fileType = type.toString().toLowerCase();
            }
        } catch (Exception e) {
            e.printStackTrace();
            fileType = "";
        }
        try {
            o = uploadDetailInputStream(file.getInputStream(), fileName, fileType, fileSize);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return o;
    }

    public ResFileDTO uploadDetailInputStream(InputStream in, String fileName, String fileType, long fileSize) {
        String uuidFileName = FileUtil.getFileUUIDName(fileName, fileType);
        String fileUrl = this.minio.getFileUrlPrefix() + this.minio.getDiskName() + uuidFileName;
        String md5key = uploadFile(in, uuidFileName, fileSize, this.minio.getBucketName(),
                "", fileName);
        ResFileDTO o = new ResFileDTO();
        if (md5key != null) {
            o.setFileType(fileType);
            o.setFileName(fileName);
            o.setCFileName(uuidFileName);
            o.setFileUrl(fileUrl);
            o.setFileSize(fileSize);
            o.setMd5key(md5key);
        }
        return o;
    }

}